<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>K-HOUSE Karaoke Lounge & Suites</title>
  <link rel="stylesheet" type="text/css" href="styles/main.css" media="all">
  <link rel="stylesheet" type="text/css" href="styles/mobile.css">
  <link rel="stylesheet" type="text/css" href="styles/desktop.css">
  <script src="scripts/jquery-3.2.1.min.js" type="text/javascript"></script>
  <script src="scripts/lander.js" type="text/javascript"></script>
</head>

<body>
  <div id="window_wrapper">
    <div class="landing_container">
      <div id="landing_img">
        <img src="./images/KH-logo.png" alt="K-House Logo">
      </div>
      <div id="landing_nav">
        <ul id="nav_list">
          <li class="nav_item"><a class="nav_link" href="about.php"><img src="./images/icons/info.png" alt="About Icon"><p>About</p></a></li>
          <li class="nav_item"><a class="nav_link" href="karaoke.php"><img src="./images/icons/microphone.png" alt="Karaoke Icon"><p>Karaoke</p></a></li>
          <li class="nav_item"><a class="nav_link" href="menu.php"><img src="./images/icons/menu.png" alt="Menu Icon"><p>Menu</p></a></li>
          <li class="nav_item"><a class="nav_link" href="deals.php"><img src="./images/icons/deals.png" alt="Deals Icon"><p>Deals</p></a></li>
          <li class="nav_item"><a class="nav_link" href="contact.php"><img src="./images/icons/phone.png" alt="Contact Icon"><p>Contact</p></a></li>
        </ul>
      </div>
    </div>
    <!-- <img id="welcome" src="./images/welcomebar.png" alt="Welcome Graphic"> -->
    <div id="landing_body">
      <div id="quote_wrapper">
        <p>The premier karaoke destination in Central NY - equiped with a full bar &
          lounge and 11 unique private suites.</p>
        <div class="button_wrapper">
          <a class="button" href="contact.php">Book Now!</a>
        </div>
      </div>
    </div>
  </div>

  <?php include("includes/footer.php"); ?>
</body>

</html>
