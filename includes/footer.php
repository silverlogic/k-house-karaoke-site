<footer>
  <div class="row">
    <div class="col">
      <ul class="footer_list">
        <li class="footer_item"><a class="footer_link" href="about.php">About</a></li>
        <li class="footer_item"><a class="footer_link" href="karaoke.php">Karaoke</a></li>
        <li class="footer_item"><a class="footer_link" href="menu.php">Menu</a></li>
        <li class="footer_item"><a class="footer_link" href="deals.php">Deals</a></li>
        <li class="footer_item"><a class="footer_link" href="contact.php">Contact</a></li>
      </ul>
    </div>

    <div class="col">
      <h3>Phone:</h3>
      <p>(607)339-8981</p>

      <h3>Address:</h3>
      <p>15 Catherwood Rd. <br/>
        Ithaca, NY 14850</p>
    </div>

    <div class="col">
      <h3>Hours:</h3>
      <p class="footer_item">Tuesday-Saturday <br/>
      4pm-1am or later</p>

      <p class="footer_item">Closed Sundays and Mondays <br/>
        Available for private parties and events</p>

      <p><a target="_blank" href="http://icons8.com">Icon Source</a>, Image Sources</p>
    </div>
  </div>
  <div class="row">
    <div id="icons">
      <a target="_blank" href="https://pro.iconosquare.com/igfeed/trkclic/771898992832221/1488629597/https%253A%257C%257Cinstagram.com%257Ckhousekaraoke"><img class="icon" src="../images/icons/instagram.png" alt="Instagram Icon"></a>
      <a target="_blank" href="https://www.facebook.com/khousekaraoke/"><img class="icon" src="../images/icons/facebook.png" alt="Facebook Icon"></a>
      <a target="_blank" href="https://www.youtube.com/user/khousekaraoke"><img class="icon" src="../images/icons/youtube.png" alt="Youtube Icon"></a>
    </div>
  </div>
</footer>
