<a href="index.php"><img class="nav_img" src="../images/KH-logo-k.png" alt="K-House Logo"/></a>
<nav class="nav_wrapper">
  <ul class="nav_list">
    <li class="nav_item <?php if($about) { echo($ACTIVE_CLASS);}?>"><a class="nav_link" href="about.php">About</a></li>
    <li class="nav_item <?php if($karaoke) { echo($ACTIVE_CLASS);}?>"><a class="nav_link" href="karaoke.php">Karaoke</a></li>
    <li class="nav_item <?php if($menu) { echo($ACTIVE_CLASS);}?>"><a class="nav_link" href="menu.php">Menu</a></li>
    <li class="nav_item <?php if($deals) { echo($ACTIVE_CLASS);}?>"><a class="nav_link" href="deals.php">Deals</a></li>
    <li class="nav_item <?php if($contact) { echo($ACTIVE_CLASS);}?>"><a class="nav_link" href="contact.php">Contact</a></li>
  </ul>
</nav>
