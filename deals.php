<?php
  $ACTIVE_CLASS = "active";
  $about = false;
  $karaoke = false;
  $menu = false;
  $deals = true;
  $contact = false;
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>K-HOUSE Karaoke Lounge & Suites</title>
  <link rel="stylesheet" type="text/css" href="styles/main.css" media="all">
  <link rel="stylesheet" type="text/css" href="styles/mobile.css">
  <link rel="stylesheet" type="text/css" href="styles/desktop.css">
  <script src="scripts/jquery-3.2.1.min.js" type="text/javascript"></script>
</head>

<body>
  <?php include("includes/nav.php"); ?>

  <h1 class="section_title">Kid's Deal</h1>

  <div id="kids_wrapper">
    <div class="kids_column" id="left_kids_column">
      <p> FREE 3rd Hour with 2 Hour Reservation </p>
      <p> FREE Goodie Bags </p>
      <p> FREE Additional Karaoke Suite for Adults </p>
      <p> *Applies to kids 16 and under for parties booked
      between 4 PM and 8 PM* </p>
    </div>
    <div class="kids_column">
      <img src="./images/kidkaraoke.jpg" alt="Image of Child Singing">
    </div>
  </div>

  <div class="purple_section">
    <h1 class="section_title">Weekly Deals</h1>
    <div id="deals_wrapper">
      <div class="deals">
        <div class="img_wrapper">
          <img alt="deal1" src="./images/deal1.jpg" />
        </div>
        <div class="img_wrapper">
          <img alt="deal2" src="./images/deal2.jpg" />
        </div>
      </div>

      <div class="deals">
        <div class="img_wrapper">
          <img alt="deal3" src="./images/deal3.jpg" />
        </div>
        <div class="img_wrapper">
          <img alt="deal4" src="./images/deal4.jpg" />
        </div>
      </div>
    </div>
  </div>

  <?php include("includes/footer.php"); ?>
</body>

</html>
