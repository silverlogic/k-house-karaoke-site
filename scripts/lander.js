$(document).ready(function() {
  var height = $(window).height();
  var width = $(window).width();

  $("#window_wrapper").css("height", height);
  $("#window_wrapper").css("width", width);

  $(window).resize( function() {
    $("#window_wrapper").css("height", $(window).height());
    $("#window_wrapper").css("width", $(window).width());
  })

})
