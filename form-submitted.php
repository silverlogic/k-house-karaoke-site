<?php
  session_start();
  $name = $_SESSION['name'];
  $email = $_SESSION['email'];
  unset($_SESSION['email']);
  $phone = $_SESSION['phone'];
  unset($_SESSION['phone']);
  $reason = $_SESSION['reason'];
  $message = $_SESSION['message'];

  $ACTIVE_CLASS = "active";
  $about = false;
  $karaoke = false;
  $menu = false;
  $deals = false;
  $contact = true;
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>K-HOUSE Karaoke Lounge & Suites</title>
  <link rel="stylesheet" type="text/css" href="styles/main.css" media="all">
  <link rel="stylesheet" type="text/css" href="styles/mobile.css">
  <link rel="stylesheet" type="text/css" href="styles/desktop.css">
  <script src="scripts/jquery-3.2.1.min.js" type="text/javascript"></script>
</head>

<body>
  <?php include("includes/nav.php"); ?>
  <section id="contact_section">
    <h1 class="section_title">Get in Touch!</h1>

    <div id="contact_wrapper">
      <div class="contact_column" id="left">
        <div id="submit_receipt">
          <h2>Form submitted!</h2>
          <p>Name: <?php echo( htmlspecialchars($name));?></p>
          <p>Phone: <?php echo( htmlspecialchars($phone));?></p>
          <p>Email: <?php echo( htmlspecialchars($email));?></p>
          <p>Reason for Contacting: <?php echo( htmlspecialchars($reason));?></p>
          <p>Message: <?php echo( htmlspecialchars($message));?></p>
        </div>
      </div>
    <div class="contact_column" id="right">
      <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12745.949366664518!2d-76.48865249164017!3d42.48419445793214!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xdbd1b4eaeedb2fa4!2sK-HOUSE+Karaoke+Lounge+%26+Suites!5e0!3m2!1sen!2sus!4v1511728572076" allowfullscreen></iframe>
      <div id="info_wrapper">
        <div class="info">
          <h2 id="info_phone"> Phone: </h2>
          <p> (607)339-8981 </p>
        </div>
        <div class="info">
          <h2> Address: </h2>
          <p> 15 Catherwood Road </p>
          <p> Ithaca, NY 14850 </p>
        </div>
      </div>
    </div>

  </div>
</section>

  <?php include("includes/footer.php"); ?>
</body>

</html>
