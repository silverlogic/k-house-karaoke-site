<?php
  $ACTIVE_CLASS = "active";
  $about = true;
  $karaoke = false;
  $menu = false;
  $deals = false;
  $contact = false;
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>K-HOUSE Karaoke Lounge & Suites</title>
  <link rel="stylesheet" type="text/css" href="styles/main.css" media="all">
  <link rel="stylesheet" type="text/css" href="styles/mobile.css">
  <link rel="stylesheet" type="text/css" href="styles/desktop.css">
  <!-- Load jQuery -->
  <script src="scripts/jquery-3.2.1.min.js"></script>
  <!-- Load slideshow script-->
  <script src="scripts/slideshow.js" type="text/javascript"></script>
</head>

<body>
  <?php include("includes/nav.php"); ?>

  <section id="about_header">\
    <div id="about_info_wrapper">
      <div id="about_info">
        <p>(607)-339-8981 </p>
        <p>15 Catherwood Rd.<br/>
        Ithaca, NY 14850 </p>
        <p>Tuesday-Saturday <br/>
        4pm-1am or later</p>
        <p>Closed Sundays & Mondays <br/>
          (Available for private <br/>parties and events)</p>
      </div>
    </div>
  </section>

  <section class="abouttext">
    <div id="cord_img_wrapper">
      <img id="about_cord_img" src="./images/chord.png" alt="Microphone Cord Graphic">
    </div>
    <h2 class="section_title">Features</h2>
    <div id="features_wrapper">
      <div id="mic_img_wrapper">
        <img src="./images/mic.png" alt="Microphone Icon">
      </div>

      <div id="about_list_wrapper">
        <ul id="about_list">
          <li>Over 5,000 square feet</li>
          <li>Full bar and karaoke lounge</li>
          <li>11 unique private karaoke suites</li>
          <li>Asian inspired food and drink menu</li>
          <li>State of the art karaoke systems</li>
          <li>Over 150,000 songs in 17 languages</li>
          <li> Private parties up to 250 people </li>
        </ul>
      </div>
    </div>
  </section>
  <section class="purple_section">
    <h2 class="section_title" id="about_title">Gallery</h2>

    <div id="slideshow_wrapper">
      <div class="slide_button_container_right">
        <img class="slide_button icon" id="right" src="./images/icons/right.png" alt="Next Slide Button">
      </div>

      <div id="image_container">
        <img id="current_img" src="images/gallery/slide3.jpg" alt="K-House Picture">
      </div>

      <div class="slide_button_container_left">
        <img class="slide_button icon" id="left" src="./images/icons/left.png" alt="Previous Slide Button">
      </div>
    </div>

  </section>
  <?php include("includes/footer.php"); ?>
</body>

</html>
