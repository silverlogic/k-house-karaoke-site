<?php
  $ACTIVE_CLASS = "active";
  $about = false;
  $karaoke = true;
  $menu = false;
  $deals = false;
  $contact = false;
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>K-HOUSE Karaoke Lounge & Suites</title>
  <link rel="stylesheet" type="text/css" href="styles/main.css" media="all">
  <link rel="stylesheet" type="text/css" href="styles/mobile.css">
  <link rel="stylesheet" type="text/css" href="styles/desktop.css">
  <script src="scripts/jquery-3.2.1.min.js" type="text/javascript"></script>
</head>

<body>
  <?php include("includes/nav.php"); ?>
  <h1 class="section_title">Rates</h1>
  <div id="suite_rates">

    <div class="karaoke_img_wrapper">
      <img id = "room" src="./images/room.png" alt="room Icon">
    </div>

    <div class = "karaoke_li_wrapper">
      <h2> Suite Rates </h2>
      <ul>
        <li> 1-10 people: $7/hour per person </li>
        <li> 11-20 people: $6/hour per person </li>
        <li> 21-35 people: $5/hour per person </li>
      </ul>
    </div>

    <div class="karaoke_img_wrapper">
      <img id = "group" src="./images/crowd.png" alt="crowd Icon">
    </div>

    <div class = "karaoke_li_wrapper">
      <h2> Large Groups </h2>
      *Get 3 hours for the price of 2 hours*
      <ul>
        <li> 30-45 people: $350 </li>
        <li> 45-60 people: $500 </li>
        <li> 61-80 people: $650 </li>
        <li> 81-100 people: $800 </li>
        <li> 101-120 people: $1,000 </li>
        <li> 121-135 people: $1,125 </li>
        <li> 136-150 people: $1,250 </li>
        <li> 151-175 people: $1,400 </li>
        <li> 176-250 people: $1,500 </li>
      </ul>
    </div>
  </div>

  <section class="purple_section">

    <h1 class="section_title">Suites</h1>

    <div id="rooms">
      <div class="rooms">
        <div class="img_wrapper">
          <img src="images/americaroom.jpg" alt="America Room" class="room">
        </div>
        <div class="img_wrapper">
          <img src="images/khouseroom.jpg" alt="K-House Room" class="room">
        </div>
      </div>
      <div class="rooms">
        <div class="img_wrapper">
          <img src="images/emojiroom.jpg" alt="Emoji Room" class="room">
        </div>
        <div class="img_wrapper">
          <img src="images/cornellroom.jpg" alt="Cornell Room" class="room">
        </div>
      </div>
    </div>
  </section>

  <?php include("includes/footer.php"); ?>
</body>

</html>
