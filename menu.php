<?php
  $ACTIVE_CLASS = "active";
  $about = false;
  $karaoke = false;
  $menu = true;
  $deals = false;
  $contact = false;
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>K-HOUSE Karaoke Lounge & Suites</title>
  <link rel="stylesheet" type="text/css" href="styles/main.css" media="all">
  <link rel="stylesheet" type="text/css" href="styles/mobile.css">
  <link rel="stylesheet" type="text/css" href="styles/desktop.css">
  <script src="scripts/jquery-3.2.1.min.js" type="text/javascript"></script>
</head>

<body>
  <?php include("includes/nav.php"); ?>

  <div id="menu_wrapper">
    <h1 class="section_title">K-House Menu</h1>
    <h2 class="subtitle" id="menu_subtitle">Book today and try our delicious, Asian inspired cuisine!</h2>
    <img class="menu_img center" src="images/menu2.jpg" alt="K-House Menu part 1">
    <img class="menu_img center" src="images/menu1.jpg" alt="K-House Menu part 2">

    <div id="download_wrapper">
      <h2 class="subtitle"> Click to download menu</h1>
      <a id="download" target="_blank" href="./files/menu.pdf" download><img class="icon" id="center_img" src="../images/icons/download.png" alt="Download Icon"></a>
    </div>
  </div>

  <?php include("includes/footer.php"); ?>
</body>

</html>
