<?php
 $HIDDEN_ERROR_CLASS = "hiddenError";
 $submit = $_REQUEST["submit"];

 $ACTIVE_CLASS = "active";
 $about = false;
 $karaoke = false;
 $menu = false;
 $deals = false;
 $contact = true;

 if (isset($submit)) {

   $name = $_REQUEST["name"];
   $email = $_REQUEST["email"];
   $phone = $_REQUEST["phone"];
   $reason = $_REQUEST["reason"];
   $message = $_REQUEST["message"];

   if ( !empty($name) ){
     $nameValid = true;
   } else {
     $nameValid = false;
   }

   if ( !empty($email) ){
     if ( filter_var($email, FILTER_VALIDATE_EMAIL) ) {
       $emailValid = true;
     } else {
       $emailValid = false;
     }
   } else {
     $emailValid = false;
   }

   if ( !empty($message) ){
     $messageValid = true;
   } else {
     $messageValid = false;
   }

   $formValid = $nameValid && $emailValid && $messageValid;

   if ($formValid) {
     session_start();
     $_SESSION['name'] = $name;
     $_SESSION['email'] = $email;
     $_SESSION['phone'] = $phone;
     $_SESSION['reason'] = $reason;
     $_SESSION['message'] = $message;
     header("Location: form-submitted.php");
     return;
   }
 } else {
   $nameValid = true;
   $emailValid = true;
   $messageValid = true;
   $formValid = true;
 }
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>K-HOUSE Karaoke Lounge & Suites</title>
  <link rel="stylesheet" type="text/css" href="styles/main.css" media="all">
  <link rel="stylesheet" type="text/css" href="styles/mobile.css">
  <link rel="stylesheet" type="text/css" href="styles/desktop.css">
  <script src="scripts/jquery-3.2.1.min.js" type="text/javascript"></script>
</head>

<body>
  <?php include("includes/nav.php"); ?>
  <h1 class="section_title">Get in Touch!</h1>
  <h2 class="subtitle">Questions? Ready to Book? Fill out our contact form below!</h2>
  <section id="contact_section">


    <div id="contact_wrapper">
      <div class="contact_column" id="left">
        <form action="contact.php#contact_wrapper" method="post" id="mainForm" novalidate>
          <div class="errorContainer <?php if ($formValid) { echo($HIDDEN_ERROR_CLASS); } ?>">
            <p class="<?php if ($formValid) { echo($HIDDEN_ERROR_CLASS); } ?>">One or more required fields are empty.</p>
          </div>
          <div class="form_element row">
            <label for="name">Name: <span class="required">*</span></label>
            <input class="errorContainer <?php if ($nameValid) { echo($HIDDEN_ERROR_CLASS);} ?>"
            id="name" type="text" name="name" value="<?php echo($name);?>" required/>
          </div>

          <div class="form_element row">
            <label for="phone">Phone:</label>
            <input id="phone" name="phone" type="text" value="<?php echo($phone);?>" required/>
          </div>

          <div class="form_element row">
            <label for="email">Email: <span class="required">*</span></label>
            <input class="errorContainer <?php if ($emailValid) { echo($HIDDEN_ERROR_CLASS);} ?>"
            id="email" name="email" type="email" value="<?php echo($email);?>" required/>
          </div>

          <div class="form_element column">
            <label for="reason">Reason for Contacting:</label>
            <input id="reason" list="type" name="reason"  value="<?php echo($reason);?>" required>
            <datalist id="type">
              <option value="Plan Event">
              <option value="Get Rates">
              <option value="General Info">
              <option value="Other">
            </datalist>
          </div>

          <div class="form_element column">
            <label for="message">Message: <span class="required">*</span></label>
            <textarea class="errorContainer <?php if ($messageValid) { echo($HIDDEN_ERROR_CLASS);} ?>"
              id="message" name="message" required><?php echo($message);?></textarea>
          </div>

          <div class="form_element button_wrapper">
            <button class="button" type="submit" name="submit">Submit</button>
          </div>
        </form>
    </div>
    <div class="contact_column" id="right">
      <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12745.949366664518!2d-76.48865249164017!3d42.48419445793214!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xdbd1b4eaeedb2fa4!2sK-HOUSE+Karaoke+Lounge+%26+Suites!5e0!3m2!1sen!2sus!4v1511728572076" allowfullscreen></iframe>
      <div id="info_wrapper">
        <div class="info">
          <h2 id="info_phone"> Phone: </h2>
          <p> (607)339-8981 </p>
        </div>
        <div class="info">
          <h2> Address: </h2>
          <p> 15 Catherwood Road </p>
          <p> Ithaca, NY 14850 </p>
        </div>
      </div>
    </div>

  </div>
</section>

  <?php include("includes/footer.php"); ?>
</body>

</html>
